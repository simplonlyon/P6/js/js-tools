export class Person {
    
    constructor(name, birthdate, address) {
        this.name = name;
        this.birthdate = birthdate;
        this.address = address;
    }
}